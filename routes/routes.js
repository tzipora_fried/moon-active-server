const express = require('express');


const app = express.Router();
const repository = require('../respositories/promotionRepository');

app.get('/', (req, res) => {
  repository.findAll().then((promotions) => {
    res.json(promotions);
  }).catch((error) =>
      console.log(error)
  );
});

app.get('/paginate', (req, res) => {
  const {skip, take} = req.query;
  repository.paginate(skip, take).then((promotions) => {
    res.json(promotions);
  }).catch((error) =>
      console.log(error)
  );
});

app.post('/create', (req, res) => {
  const promotion = req.body;
  repository.create(promotion).then((promotion) => {
    res.json(promotion);
  }).catch((error) => console.log(error));
});

app.delete('/:id', (req, res) => {
  const { id } = req.params;
  repository.deleteById(id).then((ok) => {
    console.log(ok);
    console.log(`Deleted record with id: ${id}`);
    res.status(200).json([]);
  }).catch((error) => console.log(error));
});


app.put('/:id', (req, res) => {
  const { id } = req.params;
  const promotion = req.body;
  repository.updateById(id, promotion)
    .then(res.status(200).json([]))
    .catch((error) => console.log(error));
});
module.exports = app;